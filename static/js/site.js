/* global jQuery */
(function($) {
    $(document).ready(devmenu);

    function devmenu() {
        $('#menu-toggle').click(function() {
            $('#primary-menu').toggleClass('menu-open');
        });
    }
})(jQuery)