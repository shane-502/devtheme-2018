# Dads Eye View Site  

## Setup  
Before you can "install", you'll need a few tools.  

- `PrePros 6`  
- `Timber` plugin  
- `WP Pusher` Plugin  
- `Advanced Custom Fields Pro` plugin  
If you want to use Visual Studio Code, I have included my 'settings.json' file and workspace/theme setup inside the .vscode folder.  
If you choose to use my theme; VSCode will throw an error if you try to use '!important' because having to use '!important' means your code is out of control.  
The color theme I use is 'Liqube Dark Code' and can be changed via the 'settings.json' file  
  
You will need a few extensions (Ext Name - Dev Name)  
-> ESLint - Dirk Baeumer  
-> HTML Snippets - Mohamed Abusaid  
-> Path Intellisense - Christian Kohler  
-> SCSS Intellisense - mrmlnc  
-> Twig Language - mblode  
-> Seti-Icons - qinjia (Icon tabs and Icons in file tree)  

PrePros is used to compile CSS, minify JS, / Plugins are used for plugin things.  

### CSS  
- ALL variables go in the `_variables.scss` file.  
- There should be one SCSS file for each custom page template or custom post type and they should be named appropriately. This makes finding CSS rules really really easy. CSS class names in this file should be named appropriately for the component they reference.  
- If you reuse a pattern in more than one place, then you can consider adding it to the `_global-patterns.scss` file. These class names should be fairly generic.  
- Media Queries should be placed inline on the element they affect if at all possible. *DO NOT place all the media queries at the bottom of the file.* Yo' momma raised you better than that!  

### PHP  
- Custom Page Templates should generally have their own `.twig` files. It makes adding new code easy to reason about.  
- Single CPT templates should also have their own `.twig` files  
- When possible try to use the WordPress template heirachy before resorting to routing your templates via PHP.  
- Custom Post Types go in the `inc` folder and should also have a `.twig` file associated with it.  