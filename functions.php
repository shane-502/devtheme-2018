<?php
// check for Timber
if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

// change views dir to templates
Timber::$locations = __DIR__ . '/templates';

class DadsEyeViewSite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );

		add_action( 'init', function(){
			add_editor_style('style.css' );
		} );

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['year'] = date('Y');
		$context['is_home'] = is_home();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );

		return $context;
	}

	function after_setup_theme() {
		// Menu dashboard support
		register_nav_menu( 'primary', 'Main Navigation' );

		// Image sizes
		add_image_size( 'xlarge', 2880, 2000 );
		add_image_size( 'large-square', 500, 500, true );
	}

	function enqueue_scripts() {
		// Dependencies
		wp_enqueue_style( 'dev-css', get_stylesheet_directory_uri() . "/style.css", array(), '20180311' );
		wp_enqueue_script( 'dev-theme', get_template_directory_uri() . "/static/js/site.js", array( 'jquery', 'underscore' ), '20160824' );
	}

	function admin_head_css() {
		?><style type="text/css">
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }
		</style><?php
	}
}

new DadsEyeViewSite();

add_action( 'admin_init', 'remove_editor_box' );
function remove_editor_box() {
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  	if( !isset( $post_id ) ) return;

	$home = get_the_title( $post_id );
	$faq_page = get_the_permalink( $post_id ); // permalink needed for [F.A.Q's title]

	if( $home == 'Home' ||
		$faq_page == 'https://dadseyeviewmhk.com/f-a-qs/'
	) {
		remove_post_type_support('page', 'editor');
  	}
}

function dev_render_primary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => false,
		// 'menu_class' => '',
		'menu_id' => 'primary-menu',
	) );
}

function load_gravity_form_1() {
	gravity_form( 1, false, false, false, '', false );
}