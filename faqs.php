<?php
/**
 * Template Name: FAQS
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$templates = array( 'faqs.twig' );

the_post(); // give access to editor box for [post.content]

Timber::render( $templates, $context );